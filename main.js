let num1 = +prompt('Enter number 1: ');
let num2 = +prompt('Enter number 2: ');
let operation = prompt('Enter operation: "+" or "-" or "*" or "/": ');

function showResult(num1, operation, num2){
    if(operation === '+'){
        return(num1+num2);
    }else if(operation === '-'){
        return(num1-num2);
    }else if(operation === '*'){
        return(num1*num2);
    }else if(operation === '/'){
        return(num1/num2);
    }
}
console.log(showResult(num1, operation, num2));